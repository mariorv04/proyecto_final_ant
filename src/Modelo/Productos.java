package Modelo;

import java.text.DecimalFormat;
public class Productos {
    int id;
    String nombre;
    double prec;
    int stock;

    public Productos() {
    }

    public Productos(int id, String nombre, double prec, int stock) {
        this.id = id;
        this.nombre = nombre;
        this.prec = prec;
        this.stock = stock;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrec() {
        return prec;
    }

    public void setPrec(double prec) {
        this.prec = prec;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}
