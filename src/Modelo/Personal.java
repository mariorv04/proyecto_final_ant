package Modelo;


public class Personal {
    int id;
    String dni;
    String nombre;
    String user;
    String pass;

    public Personal() {
    }

    public Personal(int id, String dni, String nombre, String user, String pass) {
        this.id = id;
        this.dni = dni;
        this.nombre = nombre;
        this.user = user;
        this.pass = pass;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
    
    
}
