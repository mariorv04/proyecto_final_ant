package Modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class BaseDatos {
    String driver = "oracle.jdbc.driver.OracleDriver";
    String url = "jdbc:oracle:thin:@localhost:1521:XE"; //?serverTimezone=UTC: Arreglar problemas de zona horaria
    String usuario = "Proyecto";
    String clave = "proyecto";
    
    public Connection Conectar() {
        try{
            Class.forName(driver);
            Connection xcon = DriverManager.getConnection(url, usuario, clave);
            return xcon;
        }
        catch(Exception e){
            System.out.println(e.toString());
        }
        return null;
    }
    protected String generarCodigo(String tabla, String campo) throws SQLException {
        String rpta = "";
        String sql = "select count(*) from " + tabla;
        Connection xcon = this.Conectar();
        Statement st = xcon.createStatement();
        ResultSet rs = st.executeQuery(sql);
        rs.next();
        int cant = Integer.parseInt(rs.getString(1).toString());
        if ( cant <= 0 )
            rpta = "1";
        else {
            sql = "select max(" + campo + ") from " + tabla;
            rs = st.executeQuery(sql);
            rs.next();
            cant = Integer.parseInt(rs.getString(1).toString()) + 1;
            rpta = "" + cant;
        }
        xcon.close();
        return rpta;
     }

    PreparedStatement prepareStatement(String sql) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    void close() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
