package Modelo;

import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;


public class ClienteDAO implements CRUD{
    Connection xcon;
    BaseDatos con = new BaseDatos();
    PreparedStatement ps;
    ResultSet rs;

    public Cliente listarID(String dni) {
        Cliente cl = new Cliente();
        String sql="select * from cliente where Dni=?";
        try {
            xcon = con.Conectar();
            ps=xcon.prepareStatement(sql);
            ps.setString(1, dni);
            rs=ps.executeQuery();
            while (rs.next()){
                cl.setId(rs.getInt(1));
                cl.setDni(rs.getString(2));
                cl.setNombre(rs.getString(3));
             }
        } catch (Exception e) {
        }
        return cl;
    }
    
    @Override
    public List listar() {
        List<Cliente> lista = new ArrayList<>();
        String sql = "select * from cliente";
        try {
            xcon = con.Conectar();
            ps=xcon.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()) {
                Cliente c = new Cliente();
                c.setId(rs.getInt(1));
                c.setDni(rs.getString(2));
                c.setNombre(rs.getString(3));
                lista.add(c);
            }
        } catch (Exception e) {
        }
        return lista;
    }

    @Override
    public void agregar(Object[] o){
        try {
            String xcod = con.generarCodigo("Cliente","IdCliente");
            String xdni = (String) o[0];
            String xnom = (String) o[1];
            String sql = "insert into cliente (IdCliente, Dni, Nombre) values (?,?,?)";
            xcon = con.Conectar();
            ps=xcon.prepareStatement(sql);
            ps.setObject(1, xcod);
            ps.setObject(2, xdni);
            ps.setObject(3, xnom);
            ps.executeUpdate();
            xcon.close();
        } catch (Exception e) {
            System.out.println("No se pudo agregar");
        }
    }

    @Override
    public void editar(Object[] o) {
        try {
            String xcod = (String) o[0];
            String xdni = (String) o[1];
            String xnom = (String) o[2];
            String sql = "update cliente set Dni=?, Nombre=? where IdCliente=?";
            xcon = con.Conectar();
            ps=xcon.prepareStatement(sql);
            ps.setObject(1, xdni);
            ps.setObject(2, xnom);
            ps.setObject(3, xcod);
            ps.executeUpdate();
            xcon.close();
        } catch (Exception e) {
            System.out.println("No se pudo editar");
        }
    }

    @Override
    public void eliminar(int id) {
        String sql = "delete from cliente where IdCliente=?";
        try {
            xcon = con.Conectar();
            ps=xcon.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }
    
    public void GenerarReporte() throws JRException {

        try{
            
            JasperReport jr = JasperCompileManager.compileReport("src//Reportes//TablaCliente.jrxml");
            JasperPrint jp = JasperFillManager.fillReport(jr, null, xcon);
            JasperViewer jv = new JasperViewer(jp, false);
            jv.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            jv.setTitle("Reporte de los Clientes");
            jv.setVisible(true);

        }
        catch (Exception j)
        {
            System.out.println("Mensaje de Error:"+j.getMessage());
        }
    } 
}
