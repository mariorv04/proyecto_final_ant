package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

public class VentasDAO {
    BaseDatos con = new BaseDatos();
    Connection xcon;
    PreparedStatement ps;
    ResultSet rs;
    int r = 0;
    
    public String NroVentas() {
        String serie = "";
        String sql = "SELECT max(NumeroVentas) FROM Ventas";
        try {
            xcon=con.Conectar();
            ps = xcon.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs == null) {
                return null;
            } else {
                while (rs.next()) {
                    serie = String.valueOf(rs.getInt(1)); //hace referencia al SELECT campos 1,2,3 ...
                    System.out.println("Serie: " + serie);
                }
            }
        } catch (SQLException e) {
            System.err.println("Error DAO: " + e.getMessage());
        }
        return serie;
    }
    
    public String idVentas() {
        String idv = "";
        String sql = "select max(IdVenta) from ventas";
        try {
            xcon = con.Conectar();
            ps = xcon.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                idv = rs.getString(1);
            }
        } catch (Exception e) {
        }
        return idv;
    }

    public void GuardarVentas(Object[] o){
        try {
            String xcod = con.generarCodigo("ventas", "IdVenta");
            int xidc = (int) o[0];
            int xidp = (int) o[1];
            String xfecha = (String) o[2];
            double xmonto = (double) o[3];
            String xnrov = (String) o[4];
            System.out.println(xcod+"---"+xidc+"-"+xidp+"-"+xfecha+"-"+xmonto+"-"+xnrov+"-"+"GuardarVentas");
            String sql = "insert into ventas (IdVenta, IdCliente, IdPersonal, FechaVenta, Monto, NumeroVentas) values (?,?,?,?,?,?)";
            xcon = con.Conectar();
            ps=xcon.prepareStatement(sql);
            ps.setObject(1, xcod);
            ps.setObject(2, xidc);
            ps.setObject(3, xidp);
            ps.setObject(4, xfecha);
            ps.setObject(5, xmonto);
            ps.setObject(6, xnrov);
            
            ps.executeUpdate();
            xcon.close();
        } catch (Exception e) {
            System.out.println("Error en GuardarVentas");
        }    
    }
    
    public void GuardarDetalleVentas(Object[] o){
        try {
            String xcod = con.generarCodigo("Concepto","IdConcepto");
            int xidv = (int) o[0];
            int xidp = (int) o[1];
            int xcant = (int) o[2];
            double xpre = (double) o[3];
            System.out.println(xcod+"---"+xidv+"-"+xidp+"-"+xcant+"-"+xpre+"-"+"Concepto");
            String sql = "insert into concepto (IdConcepto,IdVenta,IdProducto,Cantidad, PrecioVenta) values (?,?,?,?,?)";
            String sql2 = "insert into concepto (IdConcepto, IdVenta) values (?,?)";
            xcon = con.Conectar();
            ps=xcon.prepareStatement(sql);
            
            ps.setObject(1, xcod);
            ps.setObject(2, xidv);
            ps.setObject(3, xidp);
            ps.setObject(4, xcant);
            ps.setObject(5, xpre);
            ps.executeUpdate();
            xcon.close();
        } catch (Exception e) {
            System.out.println("Error en Concepto");
        }
    }
    
    public void GenerarReporte() throws JRException {

        try{
            
            JasperReport jr = JasperCompileManager.compileReport("src//Reportes//TablaVentas.jrxml");
            JasperPrint jp = JasperFillManager.fillReport(jr, null, xcon);
            JasperViewer jv = new JasperViewer(jp, false);
            jv.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            jv.setTitle("Reporte de Ventas");
            jv.setVisible(true);

        }
        catch (Exception j)
        {
            System.out.println("Mensaje de Error:"+j.getMessage());
        }
    }
}