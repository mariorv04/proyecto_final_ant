package Modelo;

public class Ventas {
    int id;
    int idCliente;
    int idPersonal;
    String fecha;
    double Monto;
    String nroVentas;

    public Ventas() {
    }

    public Ventas(int id, int idCliente, int idPersonal, String fecha, double Monto, String nroVentas) {
        this.id = id;
        this.idCliente = idCliente;
        this.idPersonal = idPersonal;
        this.fecha = fecha;
        this.Monto = Monto;
        this.nroVentas = nroVentas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getIdPersonal() {
        return idPersonal;
    }

    public void setIdPersonal(int idPersonal) {
        this.idPersonal = idPersonal;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public double getMonto() {
        return Monto;
    }

    public void setMonto(double Monto) {
        this.Monto = Monto;
    }

    public String getNroVentas() {
        return nroVentas;
    }

    public void setNroVentas(String nroVentas) {
        this.nroVentas = nroVentas;
    }
}
