package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;


public class ProductosDAO implements CRUD{
    int r;
    PreparedStatement ps;
    ResultSet rs;
    Connection xcon;
    BaseDatos con = new BaseDatos();
    Productos pro = new Productos();

    public int actualizarStock(int cant, int idp){
        String sql = "update producto set Stock=? where idProducto=?";
        try{
            xcon=con.Conectar();
            ps=xcon.prepareStatement(sql);
            ps.setInt(1, cant);
            ps.setInt(2, idp);
            ps.executeUpdate();
        }catch (Exception e){
        }
        return r;
    }
    
    public Productos ListarID(int id){
        Productos p = new Productos();
        String sql="select * from producto where IdProducto=?";
        try {
            xcon = con.Conectar();
            ps=xcon.prepareStatement(sql);
            ps.setInt(1, id);
            rs=ps.executeQuery();
            while (rs.next()){
                p.setId(rs.getInt(1));
                p.setNombre(rs.getString(2));
                p.setPrec(rs.getDouble(3));
                p.setStock(rs.getInt(4));
            }
        } catch (Exception e) {
        }
        return p;
    }
    
    public List listar() {
        List<Productos> lista = new ArrayList<>();
        String sql = "select * from producto";
        try {
            xcon = con.Conectar();
            ps=xcon.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()) {
                Productos pr = new Productos();
                pr.setId(rs.getInt(1));
                pr.setNombre(rs.getString(2));
                pr.setPrec(rs.getDouble(3));
                pr.setStock(rs.getInt(4));
                lista.add(pr);
            }
        } catch (Exception e) {
        }
        return lista;
    }

    @Override
    public void agregar(Object[] o){
        try {
            String xcod = con.generarCodigo("Producto","IdProducto");
            String xnom = (String) o[0];
            double xprec = (double) o[1];
            int xstock =  (int) o[2];
            String sql = "insert into producto (IdProducto, Nombre, Precio, Stock) values (?,?,?,?)";
            xcon = con.Conectar();
            ps=xcon.prepareStatement(sql);
            ps.setObject(1, xcod);
            ps.setObject(2, xnom);
            ps.setObject(3, xprec);
            ps.setObject(4, xstock);
            ps.executeUpdate();
            xcon.close();
        } catch (Exception e) {
            System.out.println("No se pudo agregar");
        }
    }

    @Override
    public void editar(Object[] o) {
        try {
            String xcod = (String) o[0];
            String xnom = (String) o[1];
            double xprec = (double) o[2];
            int xstock =  (int) o[3];
            String sql = "update producto set Nombre=?, Precio=?, Stock=? where IdProducto=?";
            xcon = con.Conectar();
            ps=xcon.prepareStatement(sql);
            ps.setObject(1, xnom);
            ps.setObject(2, xprec);
            ps.setObject(3, xstock);
            ps.setObject(4, xcod);
            ps.executeUpdate();
            xcon.close();
        } catch (Exception e) {
            System.out.println("No se pudo editar");
        }
    }

    @Override
    public void eliminar(int id) {
        String sql = "delete from producto where IdProducto=?";
        try {
            xcon = con.Conectar();
            ps=xcon.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }
    
    public void GenerarReporte() throws JRException {

        try{
            
            JasperReport jr = JasperCompileManager.compileReport("src//Reportes//TablaProducto.jrxml");
            JasperPrint jp = JasperFillManager.fillReport(jr, null, xcon);
            JasperViewer jv = new JasperViewer(jp, false);
            jv.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            jv.setTitle("Reporte de Productos");
            jv.setVisible(true);

        }
        catch (Exception j)
        {
            System.out.println("Mensaje de Error:"+j.getMessage());
        }
    }
}

