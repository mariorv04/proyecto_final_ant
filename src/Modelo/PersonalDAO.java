package Modelo;

import JFrames.crudVentas;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;


public class PersonalDAO implements CRUD{
    Connection xcon;
    BaseDatos con = new BaseDatos();
    PreparedStatement ps;
    ResultSet rs;
    
    public Login ListarID(int id){ 
        Login log = new Login();
            String sql="select * from Personal where IdPersonal=?";
            try {
                xcon = con.Conectar();
                ps=xcon.prepareStatement(sql);
                ps.setInt(1, id);
                rs=ps.executeQuery();
                while (rs.next()){
                    log.setId(rs.getInt(1));
                    log.setDni(rs.getString(2));
                    log.setNom(rs.getString(3));
                    log.setUser(rs.getString(4));
                }
            } catch (Exception e) {
            }
            return log;
    }
    
    public Login ValidarPersonal(String user, String pass) {
        Login log = new Login();
        String sql = "select * from personal where Usuario=? and Contraseña=?";
        try {
            xcon = con.Conectar();
            ps=xcon.prepareStatement(sql);
            ps.setString(1, user);
            ps.setString(2, pass);
            rs=ps.executeQuery();
            while (rs.next()){
                 log.setId(rs.getInt(1));
                 log.setDni(rs.getString(2));
                 log.setNom(rs.getString(3));
                 log.setUser(rs.getString(4));
                 log.setPass(rs.getString(5));
            }
        } catch(Exception e) {
        
        }
        return log;
    }

    @Override
    public List listar() {
        List<Personal> lista = new ArrayList<>();
        String sql = "select * from personal";
        try {
            xcon = con.Conectar();
            ps=xcon.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()) {
                Personal p = new Personal();;
                p.setId(rs.getInt(1));
                p.setDni(rs.getString(2));
                p.setNombre(rs.getString(3));
                p.setUser(rs.getString(4));
                p.setPass(rs.getString(5));
                lista.add(p);
            }
        } catch (Exception e) {
        }
        return lista;
    }

    @Override
    public void agregar(Object[] o){
        try {
            String xcod = con.generarCodigo("Personal","IdPersonal");
            String xdni = (String) o[0];
            String xnom = (String) o[1];
            String xuser = (String) o[2];
            String xpass = (String) o[3];
            String sql = "insert into personal (IdPersonal, Dni, Nombre, Usuario, Contraseña) values (?,?,?,?,?)";
            xcon = con.Conectar();
            ps=xcon.prepareStatement(sql);
            ps.setObject(1, xcod);
            ps.setObject(2, xdni);
            ps.setObject(3, xnom);
            ps.setObject(4, xuser);
            ps.setObject(5, xpass);
            ps.executeUpdate();
            xcon.close();
        } catch (Exception e) {
            System.out.println("No se pudo agregar");
        }
    }

    @Override
    public void editar(Object[] o) {
        try {
            String xcod = (String) o[0];
            String xdni = (String) o[1];
            String xnom = (String) o[2];
            String xuser = (String) o[3];
            String xpass = (String) o[4];
            String sql = "update personal set Dni=?, Nombre=?, Usuario=?, Contraseña=? where IdPersonal=?";
            xcon = con.Conectar();
            ps=xcon.prepareStatement(sql);
            ps.setObject(1, xdni);
            ps.setObject(2, xnom);
            ps.setObject(3, xuser);
            ps.setObject(4, xpass);
            ps.setObject(5, xcod);
            ps.executeUpdate();
            xcon.close();
        } catch (Exception e) {
            System.out.println("No se pudo editar");
        }
    }

    @Override
    public void eliminar(int id) {
        String sql = "delete from personal where IdPersonal=?";
        try {
            xcon = con.Conectar();
            ps=xcon.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }
    
    public void GenerarReporte() throws JRException {

        try{
            
            JasperReport jr = JasperCompileManager.compileReport("src//Reportes//TablaPersonal.jrxml");
            JasperPrint jp = JasperFillManager.fillReport(jr, null, xcon);
            JasperViewer jv = new JasperViewer(jp, false);
            jv.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            jv.setTitle("Reporte del Personal");
            jv.setVisible(true);

        }
        catch (Exception j)
        {
            System.out.println("Mensaje de Error:"+j.getMessage());
        }
    }  
}
